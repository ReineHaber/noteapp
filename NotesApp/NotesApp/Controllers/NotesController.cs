﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using NotesApp.Entity;

namespace NotesApp.Controllers
{
    [ApiController]
    [Route("api/v1")]
    public class NotesController : ControllerBase
    {
        public static List<Note> notes = new List<Note>();

      

        [HttpPost]
        [Route("create")]
        public IActionResult createNote([FromBody] CreateNoteDTO createNoteDTO)
        {
            Note note = new Note()
            {
                Title = createNoteDTO.Title,
                NoteDescription = createNoteDTO.NoteDescription
            };
           
            notes.Add(note);

            return Ok("Note created ! ");
        }

        [HttpGet]
        [Route("all")]

        public IActionResult GetNotes()
        {
            return Ok(notes);
        }


        // get note by id 

        [HttpGet]
        [Route("{id}")] //variable
        public IActionResult GetId([FromRoute] string id)
        {
            foreach(Note note in notes)
            {
                if(note.NoteId == id)
                {
                    NoteDTO noteDTO = new()
                    {
                        NoteId = note.NoteId,

                        Title = note.Title,

                        NoteDescription = note.NoteDescription
                    };

                    return Ok(noteDTO);
                }
            }
            return NotFound();
        }


        // remove a note from the list by id

        [HttpPost]
        [Route("delete")]

        public IActionResult DeleteNote(string id)
        {
            foreach(Note note in notes)
            {
                if(note.NoteId == id)
                {
                    notes.Remove(note);
                    return Ok("the note was deleted!");
                }
            }
            return NotFound();
        }

        [HttpPost]
        [Route("update/{id}")]

        public IActionResult UpdateNote([FromRoute] string id, [FromBody] UpdateNoteDTO updateNoteDTO)
        {
            foreach(Note note in notes)
            {
                if(note.NoteId == id)
                {
                    note.NoteDescription = updateNoteDTO.NoteDescription;
                    return Ok("Updated!");
                }
            }
            return NotFound();
        }
    }
}
