﻿using System.Text.Json.Serialization;

namespace NotesApp.Entity
{
    public class NoteDTO
    {
        [JsonPropertyName("id")]
        public string NoteId { get; set; }

        [JsonPropertyName("title")]
        public string Title { get; set; }

        [JsonPropertyName("noteDescription")]
        public string NoteDescription { get; set; }
    }
}
