﻿namespace NotesApp.Entity
{
    public class UpdateNoteDTO
    {
        public string NoteDescription { get; set; }
    }
}
