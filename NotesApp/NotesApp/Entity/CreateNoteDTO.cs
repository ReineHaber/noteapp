﻿using System.Text.Json.Serialization;

namespace NotesApp.Entity
{
    public class CreateNoteDTO
    {
        [JsonPropertyName("title")]
        public string Title { get; set; }

        [JsonPropertyName("noteDescription")]
        public string NoteDescription { get; set; }
    }
}
