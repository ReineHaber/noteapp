﻿using System;

public class Note
{
    public string NoteId { get; set; }
    public string Title { get; set; }
    public string NoteDescription { get; set; }
    public string CreatedDate { get; set; }
    public Note()
    {
        this.NoteId = Guid.NewGuid().ToString();
        this.CreatedDate = DateTime.Now.ToString("dd-MM-yyyy");

    }
}
